<?php 

/*
 * Local Configuration
 * @author Michael Pormento
 * @description Development Configuration
 */
 
use Warp\Data\DatabaseConfiguration;
use Warp\Utils\Enumerations\DebugMode;
use Warp\Utils\Enumerations\DatabaseVendor;

class LocalConfiguration extends Configuration
{
	public function Apply()
	{
		$this->SetPath("juicebox");
		$this->SetTimezone("UTC");
		$this->SetDebugMode(DebugMode::Development);
		$this->AddDatabase("main", new DatabaseConfiguration(DatabaseVendor::MYSQL, "127.0.0.1", "root", "", "juicebox"));
		$this->SetDatabase("main");
	}
}

?>