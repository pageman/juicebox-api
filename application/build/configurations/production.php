<?php

/*
 * Production Configuration
 * @author Jake Josol
 * @description Production Configuration
 */
 
use Warp\Data\DatabaseConfiguration;
use Warp\Utils\Enumerations\DebugMode;
use Warp\Utils\Enumerations\DatabaseVendor;

class ProductionConfiguration extends Configuration
{
	public function Apply()
	{
		$this->SetTimezone("UTC");
		$this->SetDebugMode(DebugMode::Production);
		$this->AddDatabase("main", new DatabaseConfiguration(DatabaseVendor::MYSQL, "aanhj4ii9g5t56.c2lmoqyvvouw.us-west-2.rds.amazonaws.com;port=3306", "admin", "juicebox", "juicebox"));
		$this->SetDatabase("main");
	}
}