<?php 

class MessageController extends Controller
{
	public function SendRecipeCompleteAction($parameters = null)
	{
		try
		{
			// ** Google GCM (http://www.programming-techniques.com/2014/01/google-cloud-messaging-gcm-in-android.html) ** //

			$input = Input::All();
			$title = Input::FromPost('title');
			$message = Input::FromPost('message');
			$type = Input::FromPost('type');

			// Static Data
			$title = 'Juice Complete';
			$message = 'Selected juice is successfully created. Enjoy your drink!';
			$type = 'message';

			// ** Retrieve IDS ** //
			$registration_ids = array();

			$url = 'https://android.googleapis.com/gcm/send';

			$message = array("title" => $title, "message" => $message, "type" => $type);
			
			// $gcm_details = UserGcmModel::GetQuery()
			// 	->WhereIsNull("deletedAt")
			// 	->WhereIsNotNull("gcmKey")
			// 	->WhereNotEqualTo("gcmKey", "")
			// 	->Find();

			// foreach ($gcm_details as $row) {
			// 	$registration_ids[] = $row["gcmKey"];
			// }

			$fields = array(
				// 'registration_ids' => $registration_ids,
				'registration_ids' => ['APA91bEDGep8qwdQoOpLfPqRbqSuI3QokyXdzCFzkipRABOJwnnaXoPezoBIlH58fM-xWHnbynHKiTrRqMANlDT4RpGmvmhvj_VwhYNPFIDqzNDk3qDJfK_WX2YkjVZJ3tQK_tFSmjJ4'],
				'data' => $message
			);

			$apiKey = "AIzaSyDModHKMvo8n6HF1Xs2TNfjx8zjCSl_I4Q";

			$headers = array(
				'Authorization: key=' . $apiKey,
				'Content-Type: application/json'
				);

			// Open Curl Connection
			$ch = curl_init();

			// ** Set the url, number of POST vars, POST data ** //
	        curl_setopt($ch, CURLOPT_URL, $url);
	        curl_setopt($ch, CURLOPT_POST, true);
	        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
	        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	        // Disabling SSL Certificate support temporarly
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fields));
	        
	        // $message = new MessageModel;
	        $result;

			// ** Execute ** //
			// $message->Fill($input);
   //     		$message->userID = Auth::User()->id;
			
			$result = curl_exec($ch);

			// if ($result) 
			// 	$message->response = "Success";	// "Success: " . $result;
			// else 
			// 	$message->response = "An error occurred while attempting to send a message";	// "Error: " . curl_error( $ch );

			// $message->Save();

			 // ** Close connection ** //
	        curl_close($ch);

	       	return Response::Make(200, "Success", array(
	       		"status" => "Success",
	       		"title" => "Message Sent",
	       		"message" => $message,
	       		"result" => $result)
	       	)->ToJSON();
		}
		catch (Exception $e)
		{
			return Response::Make(500, 'Error', 'An error occurred while attempting to send a message. Please report this problem.')->ToJSON();
		}
	}
}

?>