<?php 

class RecipeController extends Controller
{
	// Send MQTT
	private function MQTTPublishAction($device_topic, $value)
	{
		try
		{
			$mqtt = new phpMQTT('m11.cloudmqtt.com', 19610, "Juicebox"); //Change client name to something unique
			
			if ($mqtt->connect($clean = true, $will = NULL, $username = 'efbwlfcp', $password = 'CjcG3NQ6pMlP')) {
				$mqtt->publish($device_topic, $value, 0);
				$mqtt->close();
			}
		}
		catch (Exception $ex)
		{
			return Response::Make(500, 'Error', $ex)->ToJSON();
		}
	}

	// Receive MQTT


	public function IndexAction($parameters = null)
	{
		try
		{
			$recipe = RecipeModel::GetQuery()->Find();

			return Response::Make(200, 'Success', $recipe)->ToJSON();
		}
		catch(Exception $ex)
		{
			return Response::Make(200, 'Error', $ex)->ToJSON();
		}
	}

	public function ViewAction($parameters = null)
	{
		try
		{
			$recipeID = $parameters['recipeID'];
			$recipe = RecipeModel::GetQuery()->WhereEqualTo('id', $recipeID)->Find();

			return Response::Make(200, 'Success', $recipe)->ToJSON();
		}
		catch(Exception $ex)
		{
			return Response::Make(200, 'Error', $ex)->ToJSON();
		}
	}

	public function AddAction($parameters = null)
	{
		$userID = Input::FromPost('userID');

		// Test Values
		// $userID = 1;

		try
		{
			// User Info
			$user_statement = 
				"SELECT 
					u.id, u.firstname, u.middlename, u.lastname, u.birthdate, u.sex, u.height, u.height_unit, u.weight, u.weight_unit, u.lifestyle
				FROM juicebox._user u
				WHERE id = $userID";

			$user_parameters = array(':userID' => array('value' => $userID));
			$user_result = Database::Fetch($user_statement, $user_parameters, PDO::FETCH_ASSOC);

			$condition_statement =
				"SELECT 
					-- u.firstname, u.middlename, u.lastname, u.birthdate, u.sex, u.height, u.height_unit, u.weight, u.weight_unit, u.lifestyle,
					hc.name FROM juicebox.condition_user cu
				-- INNER JOIN  juicebox._user u 	
					-- ON cu.user_id = u.id
				INNER JOIN juicebox.recipe_condition rc
					ON rc.condition_id = cu.condition_id
				INNER JOIN juicebox.health_condition hc
					ON cu.condition_id = hc.id
				WHERE user_id = :userID";

			$condition_parameters = array(':userID' => array('value' => $userID));

			$condition_result = Database::FetchAll($condition_statement, $condition_parameters);
			$diabetes_value = 0;
			$hypertension_value = 0;
			$diarrhea_value = 0;

			foreach ($condition_result as $key => $value)
			{
				if ($valeue['name'] == 'Diabetes')
					$diabetes_value = 1;
				else if ($value['name'] == 'Hypertension')
					$hypertension_value = 1;
				else if ($value['name'] == 'Diarrhea')
					$diarrhea_value = 1;
				else {}
			}

			$user_result['diabetes'] = $diabetes_value;
			$user_result['hypertension'] = $hypertension_value;
			$user_result['diarrhea'] = $diarrhea_value;

			$mqtt_result = Response::Make(200, 'Success', $user_result)->ToJSON();

			$this->MQTTPublishAction("device/suggest", $mqtt_result);

			return $mqtt_result;
		}
		catch(Exception $ex)
		{
			return Response::Make(500, 'Error', $ex)->ToJSON();
		}
	}

	public function ReAddAction($parameters = null)
	{
		$userID = Input::FromPost('userID');
		$recipeID = Input::FromPost('recipeID');

		// Test Values
		// $userID = 1;

		try
		{
			// User Info
			$user_statement = 
				"SELECT 
					u.id, u.firstname, u.middlename, u.lastname, u.birthdate, u.sex, u.height, u.height_unit, u.weight, u.weight_unit, u.lifestyle
				FROM juicebox._user u
				WHERE id = $userID";

			$user_parameters = array(':userID' => array('value' => $userID));
			$user_result = Database::Fetch($user_statement, $user_parameters, PDO::FETCH_ASSOC);

			$condition_statement =
				// "SELECT ri.recipe_id, ri.ingredient_id, ri.container_id, ri.mix_percentage
				// 	 FROM juicebox.condition_user cu
				// INNER JOIN juicebox.recipe_condition rc
				// 	ON rc.condition_id = cu.condition_id
				// INNER JOIN juicebox.health_condition hc
				// 	ON cu.condition_id = hc.id
				// INNER JOIN juicebox.recipe_ingredient ri
				// 	ON ri.recipe_id = rc.recipe_id
				// WHERE user_id = :userID AND ri.recipe_id = :recipeID";
				"SELECT ri.recipe_id, ri.ingredient_id, ri.container_id, ri.mix_percentage FROM recipe_ingredient ri WHERE recipe_id = :recipeID";

			$condition_parameters = array(
				// ':userID' => array('value' => $userID),
				':recipeID' => array('value' => $recipeID)
			);

			$condition_result = Database::FetchAll($condition_statement, $condition_parameters);
			
			$mix_percentage_1 = 0;
			$mix_percentage_2 = 0;
			$mix_percentage_3 = 0;

			foreach ($condition_result as $key => $value)
			{
				if ($value['ingredient_id'] == 1)
				{
					$mix_percentage_1 = $value['mix_percentage'];
				}
				else if ($value['ingredient_id'] == 2)
					$mix_percentage_2 = $value['mix_percentage'];
				else if ($value['ingredient_id'] == 3)
					$mix_percentage_3 = $value['mix_percentage'];
				else {}
			}

			$user_result['recipe_id'] = $condition_result[0]['recipe_id'];
			$user_result['mix_percentage_1'] = $mix_percentage_1;
			$user_result['mix_percentage_2'] = $mix_percentage_2;
			$user_result['mix_percentage_3'] = $mix_percentage_3;

			$mqtt_result = Response::Make(200, 'Success', $user_result)->ToJSON();

			$this->MQTTPublishAction("device/control", $mqtt_result);

			return $mqtt_result;
		}
		catch(Exception $ex)
		{
			return Response::Make(500, 'Error', $ex)->ToJSON();
		}
	}

	public function ViewRecommendedRecipeAction()
	{
		try
		{
			$input = Input::FromPost();
			$recipe = RecipeModel::GetQuery()->WhereEqualTo('id', $input['id'])->First();

			return Response::Make(200, 'Success', $recipe)->ToJSON();
		}
		catch(Exception $ex)
		{
			return Response::Make(500, 'Error', $ex)->ToJSON();
		}
	}

	// MQTT Subscribe - Retrieve Recommended Recipe
	// public function ReceiveRecommendedRecipeAction($parameters = null)
	// {
	// 	$dateToday = date('Y-m-d h:i:s');
	// 	$input = Input::FromPost();
		
	// 	try
	// 	{
	// 		$message_statement = 
	// 			"INSERT INTO juicebox.gcm_message (message, created_at, updated_at, deleted_at) 
	// 			VALUES (:msg, :dateToday, :dateToday, null)";

	// 		$message_parameters = array(
	// 			":msg" => array('value' => $input['message']),
	// 			'dateToday' => array('value' => $dateToday)
	// 		);

	// 		$message_result = Database::Execute($message_statement, $message_parameters);
	// 	}
	// 	catch(Exception $ex)
	// 	{
	// 		return Response::Make(500, 'Error', $ex)->ToJSON();
	// 	}
	// }
}

?>