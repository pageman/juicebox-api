<?php

/*
 * User controller
 * @author Jake Josol
 * @description User controller
 */

use Warp\Utils\Traits\Controller\Apified;
use Warp\Utils\Traits\Controller\Crudified;

class UserController extends Controller 
{
	use Apified, Crudified;

	public function FetchUserAction()
	{
		try
		{
			$input = Input::FromPost();
			$user = UserModel::GetQuery()->WhereEqualTo('id', $input['id'])->First();
			$conditions = Database::FetchAll('
					SELECT
					health_condition.id,
					health_condition.name
					FROM health_condition
					INNER JOIN condition_user
					ON health_condition.id = condition_user.condition_id
					WHERE condition_user.user_id = :userID
				', array(
					":userID" => array("value" => $input['id'])
				));

			$user['conditions'] = $conditions;

			return Response::Make(200, 'Success', $user)->ToJSON();
		}
		catch(Exception $ex)
		{
			return Response::Make(500, 'Error', $ex)->ToJSON();
		}
	}
}

?>