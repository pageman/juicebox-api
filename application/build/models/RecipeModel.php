<?php 

use Warp\Utils\Enumerations\SystemField;

class RecipeModel extends Model
{
	protected static $source = "recipe";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has(SystemField::ID)->Increment();
		self::Has("name")->String(45);		
		self::Has("description")->String(200);		
		self::Has("img_source")->String(200);
	}
}

?>