<?php

/*
 * User model
 * @author Jake Josol
 * @description User model
 */
 
use Warp\Utils\Enumerations\SystemField;
use Warp\Utils\Enumerations\InputType;

class UserModel extends Model
{
	protected static $source = "_user";
	protected static $key = "id";
	protected static $fields = array();

	protected static function build()
	{
		self::Has(SystemField::ID)->Increment();
		self::Has("firstName");
		self::Has("middleName");
		self::Has("lastName");
		self::Has("birthdate");
		self::Has("sex");
		self::Has("height");
		self::Has("weight");
		self::Has("lifestyle");
	}
}