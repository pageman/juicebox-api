<?php

/*
 * Routes
 * @author Jake Josol
 * @description Determines the routes used by the app
 */

/****************************************
IMPORTED ROUTES
****************************************/

Router::Import("api");
Router::Import("engine");

/****************************************
GENERAL ROUTES
****************************************/

Router::Home("HomeController");
Router::None("NotFoundController");

// ** Mobile ** //
Router::Get('api/recipe', 'RecipeController@Index'); // List all recipes
Router::Get('api/recipe/int:recipeID', 'RecipeController@View'); // Show specific recipe

// ** MQTT and Mobile **//
Router::Post('api/suggested/view', 'RecipeController@ViewRecommendedRecipe'); // Receive Recommended Recipe
Router::Post('api/recipe/readd', 'RecipeController@ReAdd'); // Submit selected recipe

// Router::Post('api/recipe/add', 'RecipeController@Add'); // Submit selected recipe

// Router::Post('api/recipe/suggested', 'RecipeController@ReceiveRecommendedRecipe'); // Receive Recommended Recipe

// ** GCM ** //
Router::Post('api/message/send', 'MessageController@SendRecipeComplete');

// ** User **//
Router::Post('api/user/view', 'UserController@FetchUser');