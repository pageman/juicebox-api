<?php

/**
 * Warp Framework
 * @author Jake Josol
 * @copyright MIT License
 */

require_once __DIR__."/references/references.php";

/****************************************
INITIALIZE - Prepare the application
****************************************/
App::Meta()
	->Title("JuiceBox")
	->Subtitle("Juice Juice and Away")
	->Description("Making your life healthier");

App::Environment()
	->Add("localhost:81", "local")
	->Add("192.168.1.102:81", "local")
	->Add('juicebox.elasticbeanstalk.com', 'production');

/****************************************
START - Start the application
****************************************/
App::Start();